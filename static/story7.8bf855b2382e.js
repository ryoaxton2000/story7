$(document).ready(function(){
    if($('.toggle').prop('checked', false)) {
        $("#container").toggleClass("");
        $("#accordion h3").toggleClass("accLight")
    } else {
        $("#container").toggleClass("dark");
    }
})

$(function() {
    $(".toggle").click(function() {
        $("#container").toggleClass("dark");
        $("#accordion h3").toggleClass("accDark");
        // $("#accordion h3").css("background", "#363636");
    })
})

$(function() {
    $('#accordion > .accContent').hide();

    $('#accordion > .borderAcc').each(function() {
        $(this).click(function() {
            if($(this).next(".accContent").is(':visible')) {
                $(this).next(".accContent").slideUp();
            } else {
                $('#accordion > .accContent').hide();
                $(this).next(".accContent").slideDown();
            }
        })
    })
})