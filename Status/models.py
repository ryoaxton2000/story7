from django.db import models

# Create your models here.
class Status(models.Model):
    date = models.DateField(auto_now_add=True, null=True)
    time = models.TimeField(auto_now_add=True, null=True)
    status = models.CharField(max_length=300)