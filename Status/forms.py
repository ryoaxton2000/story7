from django import forms
from . import models

class Status_Form(forms.ModelForm):
    class Meta:
        model = models.Status
        fields = ['status']