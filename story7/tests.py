from django.test import TestCase, Client
from django.urls import resolve
from . views import story7_page

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.

c = Client()

class Story7_Test(TestCase):
    def test_ada_link(self):
        response = c.get('')
        self.assertEqual(response.status_code, 200)

    def test_link_salah(self):
        response = c.get('/hei')
        self.assertEqual(response.status_code, 404)

    def test_story7_template(self):
        response = c.get('')
        self.assertTemplateUsed(response, 'story7.html')

    def test_func_story7_page(self):
        response = resolve('/')
        self.assertEqual(response.func, story7_page)

    def test_ada_toggle_dark_mode(self):
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn('Darkmode', content)
        self.assertIn('<input class ="toggle" type="checkbox">', content)

    def test_ada_sambutan(self):
        response = c.get('')
        content = response.content.decode("utf8")
        self.assertIn("Halo! Apa kabar?", content)

    def test_ada_accordion(self):
        response = c.get('')
        content = response.content.decode("utf8")
        self.assertIn('id="accordion', content)
        self.assertIn('class="borderAcc', content)
        self.assertIn('class="accContent', content)

        count1 = content.count('class="borderAcc')
        self.assertEqual(count1, 3)
        count2 = content.count('class="accContent')
        self.assertEqual(count2, 3)

    def test_accordion_tertutup(self):
        response = c.get('')
        content = response.content.decode("utf8")

class Functional_Test(LiveServerTestCase):
    def setUp(self):
        # options = Options()
        # self.browser = webdriver.Firefox(executable_path="DRIVER/geckodriver")
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser  = webdriver.Chrome(chrome_options=chrome_options)

    
    def tearDown(self):
        self.browser.quit()

    def test_dark_mode(self):
        self.browser.get(self.live_server_url)
        button = self.browser.find_element_by_class_name('switch')
        self.assertNotIn('class="dark"', self.browser.page_source)
        self.assertIn('class="accLight"', self.browser.page_source)
        time.sleep(1)
        button.click()
        time.sleep(1)
        self.assertIn('class="dark"', self.browser.page_source)
        self.assertIn(' accDark', self.browser.page_source)

    def test_accordion(self):
        self.browser.get(self.live_server_url)
        cont1 = 'name="cont1" style="display: none;'
        cont2 = 'name="cont2" style="display: none;'
        cont3 = 'name="cont3" style="display: none;'
        self.assertIn('cont1', self.browser.page_source)
        self.assertIn('cont2', self.browser.page_source)
        self.assertIn('cont3', self.browser.page_source)

        acc1 = self.browser.find_element_by_name('acc1')
        acc1.click()
        self.assertNotIn(cont1, self.browser.page_source)
        acc1.click()
        self.assertIn('cont1', self.browser.page_source)

        acc2 = self.browser.find_element_by_name('acc2')
        acc2.click()
        self.assertNotIn(cont2, self.browser.page_source)
        acc2.click()
        self.assertIn('cont2', self.browser.page_source)

        acc3 = self.browser.find_element_by_name('acc3')
        acc3.click()
        self.assertNotIn(cont3, self.browser.page_source)
        acc3.click()
        self.assertIn('cont3', self.browser.page_source)


