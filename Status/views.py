from django.shortcuts import render
from django.shortcuts import redirect
from . forms import Status_Form
from . models import Status
from django.http import HttpResponse

# Create your views here.
def home_page(request):
    if request.method == 'POST':
        form = Status_Form(request.POST)
        if form.is_valid():
            form.save()
            return redirect('status:home')
    else:
        form = Status_Form()
    data = Status.objects.all().order_by('-date', '-time')
    context= {
        'form':form,
        'data':data
    }
    return render(request, "story6.html", context)